from django.db import models
from django.utils import timezone

class Movie(models.Model):
    title = models.CharField(max_length=255)
    description =  models.TextField()
    title_upload_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title
    
class Review(models.Model):
    author = models.CharField(max_length=40, default="anonymous")
    author_email = models.EmailField()
    review_date = models.DateTimeField(default=timezone.now)
    rate_choices = (
        (1,1),
        (2,2),
        (3,3),
        (4,4),
        (5,5)
    )
    stars = models.IntegerField(choices=rate_choices)
    comment = models.TextField(max_length=4000)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)

    def __str__(self):
        return self.movie.title